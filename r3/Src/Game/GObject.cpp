#include "GObject.h"
#include "../Graphics/objloader.h"
#include "Game.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <glm/ext.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <iostream>

extern CGame gGame;

CMeshShape::CMeshShape( const char* objFile ) : 
	dispList(0) {
	type = MESHSHAPE;
	bool res = loadOBJ( objFile, vertices, uvs, normals);
	if(!res)
		gGame.error("Couldn't load model file.", -1);
	else
		gGame.debug("Model loaded.");

	dispList=glGenLists(1);
	glNewList(dispList,GL_COMPILE);
	// activate and specify pointer to vertex array
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glVertexPointer( 3, GL_FLOAT, 0, &vertices[0] );	
		glNormalPointer( GL_FLOAT, sizeof(glm::vec3), &normals[0]);
		glTexCoordPointer( 2, GL_FLOAT, sizeof(glm::vec2), &uvs[0]);
		glDrawArrays( GL_TRIANGLES, 0, vertices.size() );
	// deactivate vertex arrays after drawing
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	glEndList();

}
CMeshShape::~CMeshShape() {
	if(dispList!=0) {
		glDeleteLists(dispList,1);
		dispList = 0;
	}
}

void CMeshShape::render( GLuint texture, glm::mat4 relMatrix ) {

	glMultMatrixf( &relMatrix[0][0] );

	if( (r < 0.0f || g < 0.0f || b < 0.0f) )
		if( texture != 0 )
			glBindTexture( GL_TEXTURE_2D, texture );
		else
			glColor3f( 1.0f, 1.0f, 1.0f);	//if the texture is missing render it white
	else glColor3f( r, g, b);

	glCallList( dispList );
}

CShape::CShape() : 
	r(-1.0f), g(-1.0f), b(-1.0f),
	ID(0) {}	//invalid def colours -> object is rendered with texture
CShape::~CShape() {}
void CShape::render( GLuint texture, glm::mat4 relMatrix ) {}

CGObject::CGObject( int setID ) :
	physics(NULL),
	vehicle(NULL),
	physics_shape(NULL)
{
	ID = setID;
}
CGObject::~CGObject(void)
{
}

//float move = 0.0f;

void CGObject::render() {
	unsigned int i;
	bool textureNumOK=true;
	bool matrixNumOK=true;
	if( shapes.size() != textures.size() ) {
		gGame.error( "Shapes vector size and textures vector size does not match in CGObject!" );
		textureNumOK = false;
	}
	if( shapes.size() != relMatrices.size() ) {
		gGame.error( "Shapes vector size and relMatrices vector size does not match in CGObject!" );
		matrixNumOK = false;
	}
	//move += 0.01; (glm::mat4)glm::translate( 0.0f, 0.0f, move); //
	glm::mat4 modelMatrix = getMatrix();
	glMultMatrixf( &modelMatrix[0][0] );

	if( matrixNumOK && textureNumOK ) {
		for(i=0; i<shapes.size(); i++) {
			glPushMatrix();
			shapes.at(i)->render( textures.at(i), relMatrices.at(i) );
			glPopMatrix();
		}
	} else if( matrixNumOK && !textureNumOK ) {
		for(i=0; i<shapes.size(); i++) {
			glPushMatrix();
			shapes.at(i)->render( 0, relMatrices.at(i) );
			glPopMatrix();
		}
	} else {
		for(i=0; i<shapes.size(); i++) {
			glPushMatrix();
			shapes.at(i)->render( 0, glm::mat4(1.0f) );
			glPopMatrix();
		}
	}
}

glm::mat4 CGObject::getMatrix() {
	glm::mat4 modelM(1.0f);
	if( physics != NULL) {
		PxTransform pose = physics->getGlobalPose();
		modelM = (glm::mat4)glm::translate( pose.p.x, pose.p.y, pose.p.z);
		glm::quat q;
		q = glm::quat( pose.q.w, pose.q.x, pose.q.y, pose.q.z);
		modelM = modelM * glm::toMat4(q);
	} else if( vehicle != NULL ) {
		PxRigidDynamic* body = vehicle->mCar->getRigidDynamicActor();
		PxTransform pose = body->getGlobalPose();

		modelM = (glm::mat4)glm::translate( pose.p.x, pose.p.y, pose.p.z);

		glm::quat q;
		q = glm::quat( pose.q.w, pose.q.x, pose.q.y, pose.q.z);
		modelM = modelM * glm::toMat4(q);

		//PxVehicleWheelsSimData* wheels = &vehicle->mCar->mWheelsSimData;
		//PxVehicleWheelData wheel_data = (*wheels).getWheelData(3);
		//std::cout << "Toe: " << (wheel_data.mToeAngle*180.0f/3.14f) << "\n";
	} else if( physics_shape != NULL ) {
		PxRigidActor& main_actor = physics_shape->getActor();

		PxTransform pose = main_actor.getGlobalPose();
		PxTransform local_pose = physics_shape->getLocalPose();
		
		//modelM = (glm::mat4)glm::translate( local_pose.p.x, local_pose.p.y, local_pose.p.z);

		modelM = modelM * (glm::mat4)glm::translate( pose.p.x, pose.p.y, pose.p.z);
		//modelM = (glm::mat4)glm::translate( pose.p.x+local_pose.p.x, pose.p.y+local_pose.p.y, pose.p.z+local_pose.p.z);

		glm::quat q;
		q = glm::quat( pose.q.w, pose.q.x, pose.q.y, pose.q.z);
		//q1 = glm::quat( local_pose.q.w, local_pose.q.x, local_pose.q.y, local_pose.q.z);
		modelM = modelM * glm::toMat4(q);

		modelM = modelM * (glm::mat4)glm::translate( local_pose.p.x, local_pose.p.y, local_pose.p.z);

		glm::quat q1;
		q1 = glm::quat( local_pose.q.w, local_pose.q.x, local_pose.q.y, local_pose.q.z);
		modelM = modelM * glm::toMat4(q1);


		//modelM = modelM * (glm::mat4)glm::translate( local_pose.p.x, local_pose.p.y, local_pose.p.z);

		//q = glm::quat( local_pose.q.w, local_pose.q.x, local_pose.q.y, local_pose.q.z);
		//modelM = modelM * glm::toMat4(q);
	}
	return modelM;
}
