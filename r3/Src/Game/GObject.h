#ifndef GOBJECT_H_
#define GOBJECT_H_	0

#include <vector>
#include <glm/glm.hpp>

#ifdef _MSC_VER
	#include <Windows.h>
#endif
#include <GL/gl.h>
#include <GL/glu.h>

#include "../Physics/Vehicle.h"

enum shapeType { MESHSHAPE, BOXSHAPE };
enum shapesEnum { SCHTOP, SCHMIDDLE, SCHBOTTOM1, SCHBOTTOM2, SCHBOTTOM3, SCHBOTTOM4, SCHBOTTOM5, STIRE, SHAPE_GND };

class CShape {
public:
	CShape();
	~CShape();

	enum::shapeType type;
	int ID;

	virtual void render( GLuint texture, glm::mat4 relMatrix );

	float r,g,b;
};

class CMeshShape : public CShape {
public:
	CMeshShape( const char* objFile );
	~CMeshShape();

	void render( GLuint texture, glm::mat4 relMatrix );

	GLuint dispList;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
};

class CGObject
{
public:
	CGObject( int setID );
	~CGObject(void);

	int ID;

	//uses one of either 3
	PxRigidDynamic* physics;
	CVehicle* vehicle;
	PxShape* physics_shape;

	std::vector<CShape*> shapes;
	std::vector<glm::mat4> relMatrices;
	std::vector<GLuint> textures;

	glm::mat4 getMatrix();	//gets and converts it from the physics
	void render();
	void setPosition(glm::vec3 newPos);	//sets pos in the physics
	void setRotation( float deg, glm::vec3 axis );
	//void setMatrix( glm::mat4 newModelMatrix );
};

/*
class CVehicleObject : CGObject {
public:
	CVehicleObject(int setID) : CGObject(setID) {}
	CVehicle vehicle;

	glm::mat4 getMatrix();
};
*/

#endif