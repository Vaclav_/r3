#include "Game.h"
#include "State.h"
#include <iostream>

CGame::CGame(void) :
	debugOn(true),
	running(true),
	activeState(MENU),
	name("r3"),
	screenWidth(800),
	screenHeight(600),
	screenBpp(32)
{
	setFrameRate(50);
}

CGame::~CGame(void) {}

int CGame::init(){

    if ( SDL_Init(SDL_INIT_VIDEO) != 0 ) {
        error("Unable to initialize SDL: ", -1);
		error(SDL_GetError());
		error("\n");
        return -1;
    }

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 8);
    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

    screen = SDL_SetVideoMode( screenWidth, screenHeight, 32, SDL_OPENGL );
    if ( !screen ) {
		error("Unable to set video mode: ", -1);
		error(SDL_GetError());
		error("\n");
		return -1;
	}

	SDL_WM_SetCaption( name, NULL );

	SDL_EnableKeyRepeat( 10, 10 );

	return 0;
}

void CGame::end() {
	if( screen != NULL ) {
		SDL_FreeSurface( screen );
		screen = NULL;
	}
	SDL_Quit();
}

void CGame::error( const char* errMsg ){
	std::cout << "Error: " << errMsg << "\n";
}

void CGame::error( const char* errMsg, int howSerious ) {
	if(howSerious < 0) {
		std::cout << "Critical error: " << errMsg << "\n";
		std::cout << "Ending application...\n";
		running = 0;		//shutdown
	} else {
		std::cout << "Error: " << errMsg << "\n";
	}
}

void CGame::debug( const char* debugMsg ) {
	if( debugOn ) {
		std::cout << "Debug: " << debugMsg << "\n";
	}
}

void CGame::debug( const char* debugMsg , int debugNumber ) {
	if( debugOn ) {
		std::cout << "Debug: " << debugMsg << debugNumber << "\n";
	}
}

double CGame::setFrameRate( int newFrameRate ) {
	frameRate = newFrameRate;
	deltaTF = (double)1.0/frameRate;
	deltaTI = (int)(1000/frameRate);
	return deltaTF;
}

double CGame::dTFloat(){
	return deltaTF;
}

int CGame::dTInt() {
	return deltaTI;
}

int CGame::getFrameRate() {
	return frameRate;
}

bool CGame::isRunning() {
	return running;
}

void CGame::setRunning( bool gameIsRunning ) {
	running = gameIsRunning;
}

//Timer modul (CTimer)
CTimer::CTimer(void) {
	startTime = 0;
	started = false;
}

CTimer::~CTimer(void) {}

void CTimer::start() {
	startTime = SDL_GetTicks();
	started = true;
}

void CTimer::stop() {
	lastTime = SDL_GetTicks();
	started = false;
}

bool CTimer::timerStarted() {
	return started;
}

Uint32 CTimer::getTime() {
	return ( SDL_GetTicks() - startTime );
}

Uint32 CTimer::getStartTime() {
	return startTime;
}

void CTimer::delay( unsigned int ms ) {
	SDL_Delay( ms );
}
