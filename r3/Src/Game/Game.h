#ifndef _GAME_H_
#define _GAME_H_ 0

#include <SDL/SDL.h>

class CGame {
	public:
	CGame();
	~CGame();
	int init();	//Initialize the game
	void end();	//End the game

	void error( const char* errMsg );
	void error( const char* errMsg, int howSerious );	//-1 -> shutdown
	void debug( const char* debugMsg );
	void debug( const char* debugMsg , int debugNumber );

	bool isRunning();
	void setRunning( bool gameIsRunning );

	double setFrameRate( int newFrameRate );	//sets the framerate and the speed factor as well, returns the latter
	int getFrameRate();
	double dTFloat();
	int dTInt();

	int activeState;	//current gamestate

	SDL_Surface* screen;

	//game settings:
	public:
	int screenWidth;
	int screenHeight;
	int screenBpp; 

	private:
	const char* name;
	int frameRate;
	double deltaTF;
	int deltaTI;
	bool debugOn;
	bool running;

	//void loadSettings();	//reads settings from file
};


class CTimer {
	public:
	CTimer();
	~CTimer();
	void start();
	void stop();
	bool timerStarted();
	Uint32 getTime();
	Uint32 getStartTime();
	void delay( unsigned int ms );

	private: 
	//ticks when the timer was started
	Uint32 startTime;
	Uint32 lastTime;	//time when timer was stopped
	bool started;
};

#endif
