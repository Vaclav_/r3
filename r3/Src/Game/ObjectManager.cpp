#include "ObjectManager.h"
#include "../Graphics/objloader.h"
#include "../Graphics/Graphics.h"
#include "Game.h"

extern CGame gGame;

CObjectManager::CObjectManager(void) : 
	workingObject(0) {}
CObjectManager::~CObjectManager(void) {}

void CObjectManager::addShapeMesh( int shapeID, const char* objFile ) {

	CShape* newShape = (CShape*)new CMeshShape( objFile );
	newShape->ID = shapeID;

	shapeList.push_back(newShape);
	shapeUsageCnt.push_back(0);
}

GLuint CObjectManager::addTexture(  const char* fileName ) {

	GLuint newTex = loadPng( fileName );
	textureList.push_back( newTex );
	textureUsageCnt.push_back(0);

	return newTex;
}

CGObject* CObjectManager::addObject( int objectID ) {
	objectList.push_back( CGObject( objectID ) );
	workingObject = objectID;

	return &objectList.back();
}

void CObjectManager::bindObject( int objectID ) {
	workingObject = objectID;
}

int CObjectManager::getBoundObject() {
	return workingObject;
}

void CObjectManager::addPair( int shapeID, GLuint textureID, glm::mat4 relMatrix, int objID ) {
	if(objID == 0)
		objID = workingObject;

	bool foundObj = false;
	bool foundShape = false;
	int shapeNo=0;
	int texNo=0;
	bool foundTexture = false;
	std::list<CGObject>::iterator iterO;
	//find the object
	for( iterO = objectList.begin(); !foundObj && (iterO != objectList.end()); iterO++ ) {
		//found the object
		if( (*iterO).ID == objID ) {
			foundObj = true;
			//find the shape
			for( std::list<CShape*>::iterator iterS = shapeList.begin(); 
				!foundShape && (iterS != shapeList.end()); iterS++, shapeNo++ ) {
				//if it's that shape
				if( (*iterS)->ID == shapeID ) {
					foundShape = true;
					//add the shape to the object
					(*iterO).shapes.push_back( *iterS );
					gGame.debug("Added shape.");
					//find the associated shapeUsageCnt member
					std::list<unsigned int>::iterator iterSU = shapeUsageCnt.begin();
					int sh=0;
					while( sh != shapeNo ) { sh++; iterSU++; }
					//increment the shape's usage
					(*iterSU) += 1;
					gGame.debug("Shape usg incremented.");
				}
			} //if foundShape == false -> shape not found
			if( foundShape == false ) {
				gGame.error("Shape not found in ObjectManager's list!");
				return;
			}
			
			//find the texture
			for( std::list<GLuint>::iterator iterT = textureList.begin(); 
				!foundTexture && (iterT != textureList.end()); iterT++, texNo++ ) {
				//if that's it
				if( *iterT == textureID ) {
					//if the texture ID is 0, there's no texture, but that's not a problem
					(*iterO).textures.push_back(textureID);
					gGame.debug("Added texture.");
					//find the associated textureUsageCnt member
					std::list<unsigned int>::iterator iterTU = textureUsageCnt.begin();
					int n=0;
					while( n != texNo ) { n++; iterTU++; }
					//increment the texture's usage
					(*iterTU) += 1;
					gGame.debug("Tex usg incremented.");
				}
			} 
		}	//try another object
	}
	if( foundObj == false )
		gGame.error("Object not found in ObjectManager's list!");
	else {	//if we have the object, set the relative position/orientation as well
		iterO--;
		(*iterO).relMatrices.push_back( relMatrix );
		gGame.debug("Added matrix.");
	}
}

void CObjectManager::clearAll() {
	//delete all shapes
	//free all textures
	//delete shape and texture lists
	//delete all objects
	std::list<CShape*>::iterator iterS = shapeList.begin();
	std::list<unsigned int>::iterator iterSU = shapeUsageCnt.begin();
	while( iterS != shapeList.end() ) {
		if( (*iterS) != NULL )
			delete (*iterS);
		else gGame.error("Deleting shape, but the pointer is NULL!");
		iterS = shapeList.erase(iterS);
		iterSU = shapeUsageCnt.erase(iterSU);
	}
	std::list<unsigned int>::iterator iterT = textureList.begin();
	std::list<unsigned int>::iterator iterTU = textureUsageCnt.begin();
	while( iterT != textureList.end() ) {
		if( *iterT != 0 )
			glDeleteTextures(1, &(*iterT) );
		else gGame.error("Deleting texture, but ID is 0.");
		iterT = textureList.erase(iterT);
		iterTU = textureUsageCnt.erase(iterTU);
	}
	std::list<CGObject>::iterator iterO = objectList.begin();
	while( iterO != objectList.end() ) {
		iterO = objectList.erase(iterO);
	}

}

/*
void CObjectManager::deleteObject( int objID ) {
	bool foundObj = false;
	std::list<CGObject>::iterator iterO;
	//find the object
	for( iterO = objectList.begin(); !foundObj && (iterO != objectList.end()); iterO++ ) {
		//found the object
		if( (*iterO).ID == objID ) {
			foundObj = true;
			// for all shapes associated with the object
			int s;
			for( s=0; s<(*iterO).shapes.size(); s++ ) {
			//find the object's shape in the list
				std::list<CShape*>::iterator iterS = shapeList.begin();
				std::list<unsigned int>::iterator iterSU = shapeUsageCnt.begin();
				while( iterS != shapeList.end() ) {
					if( *iterS == (*iterO).shapes.at(s) ) {
						//decrement the shape cnt
						*iterSU -= 1;
						//if it's 0, remove the shape and the cnt too
						if( *iterSU == 0 ) {
							delete (*iterS);
							shapeList.erase(iterS);
							shapeUsageCnt.erase(iterSU);
						} else {
							iterS++;
							iterSU++;
						}
					} else {
						iterS++;
						iterSU++;
					}
				}
			}	//for all shapes of the object

			for( s=0; s<(*iterO).textures.size(); s++ ) {
			//find the object's shape in the list
				std::list<unsigned int>::iterator iterT = textureList.begin();
				std::list<unsigned int>::iterator iterTU = textureUsageCnt.begin();
				//check all textures in the object manager's list
				while( iterT != textureList.end() ) {
					if( *iterT == (*iterO).textures.at(s) ) {
						//decrement the shape cnt
						*iterTU -= 1;
						//if it's 0, remove the shape and the cnt too
						if( *iterTU == 0 ) {
							if(*iterT != 0)
								glDeleteTextures(1, &(*iterT) );
							textureList.erase(iterT);
							textureUsageCnt.erase(iterTU);
						} else {
							iterT++;
							iterTU++;
						}
					} else {
						iterT++;
						iterTU++;
					}
				}	//check all textures in the object manager's list
			}	//for all textures of the object


		}
	} 
	if ( foundObj == false ) { //
		gGame.error("CObjectManager::deleteObject: Object not found in ObjectManager's list!");
	}

	//find the object by ID
	//find it's last shape in the obj manager's shape list
	//find the associated shapeUsageCnt, decrement it
	//pop the shape in the object
	//if shapeUsageCnt == 0, delete the shape, and remove it's pointer from the manager's list
	//and delete the cnt
	//do the same for all shapes
	//do the same for all textures
	//remove the object from the list
}

void CObjectManager::clearUnused() {
	std::list<unsigned int>::iterator iterSU = shapeUsageCnt.begin();
	std::list<CShape*>::iterator iterS = shapeList.begin();
	while( (iterSU != shapeUsageCnt.end()) || (iterS != shapeList.end()) ) {
		if( *(iterSU) == 0 ) {
			if(*iterS!=NULL)
				delete (*iterS);
			shapeList.erase(iterS);
			shapeUsageCnt.erase(iterSU);
		} else {
			iterSU++;
			iterS++;
		}
	}

	std::list<unsigned int>::iterator iterTU = textureUsageCnt.begin();
	std::list<unsigned int>::iterator iterT = textureList.begin();
	while( (iterTU != textureUsageCnt.end()) || (iterT != textureList.end()) ) {
		if( *(iterTU) == 0 ) {
			if(*iterT!=0)
				glDeleteTextures(1, &(*iterT) );
			textureList.erase(iterT);
			textureUsageCnt.erase(iterTU);
		} else {
		iterTU++;
		iterT++;
		}
	}

	//check all elements of shapeUsageCnt
	//if it's null
		//delete the associated shape
		//remove the pointer from the list
	//same for textures
}
*/

void CObjectManager::render( CCamera& camera) {
	//render all objects
	std::list<CGObject>::iterator iter;
	for( iter = objectList.begin(); iter != objectList.end(); iter++ ) {
		camera.applyMatrix();
		(*iter).render();
	}
}