#ifndef _OBJECT_MANAGER_H_
#define _OBJECT_MANAGER_H_ 0

#include <list>
#include "GObject.h"
#include "../Graphics/Graphics.h"
#include <glm/glm.hpp>

class CObjectManager
{
public:
	CObjectManager(void);
	~CObjectManager(void);

	std::list<CGObject> objectList;

	//add to the object manager
	//void addShapeBox(int shapeID, float sizeX, float sizeY, float sizeZ);
	void addShapeMesh( int shapeID, const char* objFile );
	GLuint addTexture(  const char* fileName );

	//create object / add to object
	CGObject* addObject( int objectID );
	void bindObject( int objectID );
	int getBoundObject();
	void addPair( int shapeID, GLuint textureID, glm::mat4 relMatrix, int objID = 0 );
	//void deleteObject( int objID = 0 );
	//void clearUnused();
	void clearAll();

	void render( CCamera& camera);

private:
	int workingObject;
	std::list<CShape*> shapeList;
	std::list<GLuint> textureList;
	std::list<unsigned int> shapeUsageCnt;
	std::list<unsigned int> textureUsageCnt;
};

#endif
