#ifdef _MSC_VER
#include <Windows.h>
#endif

#include "../Physics/Sim.h"
#include "../Physics/ConvexObj.h"
#include "../Physics/Vehicle.h"
#include "../Physics/VehicleManager.h"

#include <PxPhysicsAPI.h>
using namespace physx;
/*
#pragma comment(lib, "PhysX3_x86.lib")
//#pragma comment(lib, "Foundation.lib") //->PhysX3Common_x86.lib instead
#pragma comment(lib, "PhysX3Extensions.lib")
#pragma comment(lib, "PhysX3Common_x86.lib")
#pragma comment(lib, "PhysXVisualDebuggerSDK.lib")
#pragma comment(lib, "PhysXProfileSDK.lib")
#pragma comment(lib, "PhysX3Cooking_x86.lib")
#pragma comment(lib, "PhysX3Vehicle.lib")
*/
#pragma comment(lib, "PhysX3CHECKED_x86.lib")
//#pragma comment(lib, "Foundation.lib") //->PhysX3Common_x86.lib instead
#pragma comment(lib, "PhysX3ExtensionsCHECKED.lib")
#pragma comment(lib, "PhysX3CommonCHECKED_x86.lib")
#pragma comment(lib, "PhysXVisualDebuggerSDKCHECKED.lib")
#pragma comment(lib, "PhysXProfileSDKCHECKED.lib")
#pragma comment(lib, "PhysX3CookingCHECKED_x86.lib")
#pragma comment(lib, "PhysX3VehicleCHECKED.lib")

#include <iostream>
void fatalError( const char* error ) {
	std::cout << "Error: " << error << "\n";
}

void debug( const char* msg ) {
	std::cout << "Debug: " << msg << "\n";
}

#ifdef _MSC_VER
#include <malloc.h>
class PxDefaultAllocator : public PxAllocatorCallback
{
    void* allocate(size_t size, const char*, const char*, int)
    {
        return _aligned_malloc(size, 16);
    }

    void deallocate(void* ptr)
    {
        _aligned_free(ptr);
    }
};
#else
#error Implement memory allocator for Linux first!
#endif


#include "PracticeState.h"
#include "Game.h"

#include "../Graphics/objloader.h"

#include "GObject.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <iostream>
extern CGame gGame;

GLuint tex2;
GLuint tireTexture=0;
CShape* shape;
CShape* sh2;
CGObject object(0);

CPracticeState::CPracticeState(void) :
	mSim(),
	mVManager( *mSim.mPhysics )
{
	nextState = S_PRACTICE;
}


CPracticeState::~CPracticeState(void)
{
}

int CPracticeState::loadState() {

	CMyFont loadingFont;
	loadingFont.load( "fonts/font0.bmp" );

	graphics.load2D();

	renderLoadingScr( loadingFont );
	SDL_ShowCursor(SDL_DISABLE);
	loadingFont.freeFont();

	graphics.load3D();
	graphics.camera.lookAt( glm::vec3( 0.0f, 12.0f, 20.0f ), glm::vec3( 0.0f, 0.0f, 0.0f ) );

	//flip it
	glm::mat4 flipMatrix(1.0f);
	//flipMatrix = (glm::mat4)glm::rotate( flipMatrix, 180.0f, 0.0f, 1.0f, 0.0f );

	GLuint curTex;
	CGObject* car_gobject = oManager.addObject( 1 );
	oManager.addShapeMesh( SCHTOP, "gfx/chassis_top.obj");
	oManager.addShapeMesh( SCHMIDDLE, "gfx/chassis_middle.obj");
	oManager.addShapeMesh( SCHBOTTOM1, "gfx/chassis_bottom1.obj");
	oManager.addShapeMesh( SCHBOTTOM2, "gfx/chassis_bottom2.obj");
	oManager.addShapeMesh( SCHBOTTOM3, "gfx/chassis_bottom3.obj");
	oManager.addShapeMesh( SCHBOTTOM4, "gfx/chassis_bottom4.obj");
	oManager.addShapeMesh( SCHBOTTOM5, "gfx/chassis_bottom5.obj");
	oManager.addShapeMesh( STIRE, "gfx/tire.obj");

	curTex = oManager.addTexture( "gfx/chassis_top.png" );
	
	//init pos
	glm::mat4 curMatrix(1.0f);
	glm::mat4 translate = (glm::mat4)glm::translate( 0.0f, 0.556081f, -0.276419f);
	curMatrix = curMatrix * translate;
	
	curMatrix = flipMatrix * curMatrix;

	oManager.addPair( SCHTOP, curTex, curMatrix );

	curTex = oManager.addTexture( "gfx/chassis_middle.png" );
	translate = (glm::mat4)glm::translate(0.0f, 0.153937f, 0.068693f);
	curMatrix = glm::mat4(1.0f);
	curMatrix = curMatrix * translate;
	curMatrix = flipMatrix * curMatrix;
	oManager.addPair( SCHMIDDLE, curTex, curMatrix );

	curTex = oManager.addTexture( "gfx/chassis_bottom1.png" );
	translate = (glm::mat4)glm::translate( 0.0f, -0.130277f, 2.092338f);
	curMatrix = glm::mat4(1.0f);
	curMatrix = curMatrix * translate;
	curMatrix = flipMatrix * curMatrix;
	oManager.addPair( SCHBOTTOM1, curTex, curMatrix );

	curTex = oManager.addTexture( "gfx/chassis_bottom2.png" );
	translate = (glm::mat4)glm::translate( 0.0f, -0.145744f, 1.396082f);
	curMatrix = glm::mat4(1.0f);
	curMatrix = curMatrix * translate;
	curMatrix = flipMatrix * curMatrix;
	oManager.addPair( SCHBOTTOM2, curTex, curMatrix );

	curTex = oManager.addTexture( "gfx/chassis_bottom3.png" );
	translate = (glm::mat4)glm::translate( 0.0f, -0.144163f, 0.02696f);
	curMatrix = glm::mat4(1.0f);
	curMatrix = curMatrix * translate;
	curMatrix = flipMatrix * curMatrix;
	oManager.addPair( SCHBOTTOM3, curTex, curMatrix );

	curTex = oManager.addTexture( "gfx/chassis_bottom4.png" );
	translate = (glm::mat4)glm::translate( 0.0f, -0.145744f, -1.338403f);
	curMatrix = glm::mat4(1.0f);
	curMatrix = curMatrix * translate;
	curMatrix = flipMatrix * curMatrix;
	oManager.addPair( SCHBOTTOM4, curTex, curMatrix );

	curTex = oManager.addTexture( "gfx/chassis_bottom5.png" );
	translate = (glm::mat4)glm::translate( 0.0f, -0.144089f, -1.96925f);
	curMatrix = glm::mat4(1.0f);
	curMatrix = curMatrix * translate;
	curMatrix = flipMatrix * curMatrix;
	oManager.addPair( SCHBOTTOM5, curTex, curMatrix );

	////load GND////
	CGObject* gnd_object = oManager.addObject( 2 );
	oManager.bindObject(2);
	//curMatrix = (glm::mat4)glm::translate( 0.0f, -1.0f, 0.0f);
	oManager.addShapeMesh( SHAPE_GND, "gfx/gnd.obj");
	int tx = oManager.addTexture( "gfx/gnd.png" );
	oManager.addPair( SHAPE_GND, tx, curMatrix );
	////
	
	gGame.debug("Loading OK\n.");

	/*
	shape = (CShape*)new CMeshShape( "gfx/chassis_top.obj" );
	sh2 = (CShape*)new CMeshShape( "gfx/chassis_middle.obj" );
	tireTexture = loadPng( "gfx/chassis_top.png" );
	tex2 = loadPng( "gfx/chassis_middle.png" );
	*/
/*
	//set ID too
	object.shapes.push_back(shape);
	object.textures.push_back(tireTexture);
	object.relMatrices.push_back( mat );


	glm::mat4 mat2(1.0f);
	glm::mat4 translate2 = (glm::mat4)glm::translate( 0.0f, 0.153937f, 0.068693f);
	mat2 = mat2 * translate2;

	glm::mat4 rot2(1.0f);
	rot2 = (glm::mat4)glm::rotate( rot2, 180.0f, 0.0f, 1.0f, 0.0f );
	mat2 = rot2*mat2;

	object.shapes.push_back(sh2);
	object.textures.push_back(tex2);
	object.relMatrices.push_back( mat2 );
*/

	SDL_WarpMouse( gGame.screenWidth/2, gGame.screenHeight/2 );

	if( mSim.onInit() < 0 ) {
		gGame.error( "mSim.onInit() has failed!");
		return(-1);
	}

	mSim.createActors();

	//visual debugger
	mSim.PVDInit();

	CConvexObj gnd( "gfx/gnd.obj", mSim.mPhysics, mSim.mScene, mSim.mCooking );
	if( gnd_object != NULL ) {
		gnd_object->physics = gnd.mActor;
	}

	mVManager.createStandardMaterials( *mSim.mPhysics );	
	mVManager.create( *mSim.mPhysics, *mSim.mScene, *mSim.mCooking);

	car_gobject->vehicle = &mVManager.myVehicle;



	PxShape* carShapes[11]; //!!! make this one to be dynamically allocated, carShapes[numChassisMeshes+4]
	const PxVehicleWheels& vehicle = *mVManager.myVehicle.mCar;
	const PxU32 numShapes=vehicle.getRigidDynamicActor()->getNbShapes();
	vehicle.getRigidDynamicActor()->getShapes( carShapes, numShapes);

	//FL
	CGObject* tire_object = oManager.addObject(3);
	oManager.bindObject(3);
	curTex = oManager.addTexture( "gfx/tire.png" );
	translate = glm::mat4(1.0f);//(glm::mat4)glm::translate( -0.822839f, -0.429795f, 1.407236f);
	curMatrix = glm::mat4(1.0f);
	curMatrix = curMatrix * translate;
	curMatrix = flipMatrix * curMatrix;
	oManager.addPair( STIRE, curTex, curMatrix );
	tire_object->physics_shape = carShapes[0];//mVManager.myVehicle.mWheelShapes.at(0);

	//FR
	tire_object = oManager.addObject(4);
	oManager.bindObject(4);
	//translate = (glm::mat4)glm::translate( 0.822839f, -0.429795f, 1.407236f);
	curMatrix = glm::mat4(1.0f);
	curMatrix = curMatrix * translate;
	curMatrix = flipMatrix * curMatrix;
	oManager.addPair( STIRE, curTex, curMatrix );
	tire_object->physics_shape = carShapes[1];

	//RR
	tire_object = oManager.addObject(5);
	oManager.bindObject(5);
	//translate = (glm::mat4)glm::translate( -0.822839f, -0.429795f, -1.343002f);
	curMatrix = glm::mat4(1.0f);
	curMatrix = curMatrix * translate;
	curMatrix = flipMatrix * curMatrix;
	oManager.addPair( STIRE, curTex, curMatrix );
	tire_object->physics_shape = carShapes[2];

	//RL
	tire_object = oManager.addObject(6);
	oManager.bindObject(6);
	//translate = (glm::mat4)glm::translate( 0.822839f, -0.429795f, -1.343002f);
	curMatrix = glm::mat4(1.0f);
	curMatrix = curMatrix * translate;
	curMatrix = flipMatrix * curMatrix;
	oManager.addPair( STIRE, curTex, curMatrix );
	tire_object->physics_shape = carShapes[3];

	return 0;
}

void CPracticeState::leaveState() {


	oManager.clearAll();

	/*glDeleteTextures(1, &tireTexture );
	if( shape != NULL ) {
		delete shape;
		shape = NULL;
	}
	object.shapes.pop_back();
	object.textures.pop_back();
	object.relMatrices.pop_back();
	object.shapes.pop_back();
	object.textures.pop_back();
	object.relMatrices.pop_back();*/

	if( mVManager.mSqData != NULL )
		mVManager.mSqData->free();

	//release alls stuff here
	mSim.release();
}

void CPracticeState::handleEvent( SDL_Event* ev ) {
	switch (ev->type) {
		case SDL_QUIT:
			gGame.setRunning(false);
			break;
		case SDL_KEYDOWN:
			if( ev->key.keysym.sym == SDLK_ESCAPE )
				nextState = MENU;
			break;
		default:
			break;
	}
}

//int i=1;
int prevX=400;
int prevY=300;


void CPracticeState::step() {

	Uint8 *keystate = SDL_GetKeyState(NULL);
	bool accel;
	if ( keystate[SDLK_UP] )
		accel = true;
	else
		accel = false;
	bool brake;
	if ( keystate[SDLK_DOWN] )
		brake = true;
	else
		brake = false;

	bool handbrake;
	if ( keystate[SDLK_SPACE] )
		handbrake = true;
	else
		handbrake = false;

	bool steerleft;
	bool steerright;
	if ( keystate[SDLK_LEFT] )
		steerright=true;
	else
		steerright = false;

	if( keystate[SDLK_RIGHT] )
		steerleft = true;
	else
		steerleft = false;

	bool gearup;
	if( keystate[SDLK_a] )
		gearup = true;
	else
		gearup = false;
	bool geardown;
	if( keystate[SDLK_y] )
		geardown = true;
	else
		geardown = false;

	mVManager.controller.setCarKeyboardInputs( accel, brake, handbrake, steerleft,  steerright,
		gearup, geardown);


	int curX, curY;
	SDL_GetMouseState( &curX, &curY);

	glm::vec4 axisX(1.0f, 0.0f, 0.0f, 1.0f);
	glm::vec4 axisY(0.0f, 1.0f, 0.0f, 1.0f);

	//vert = vert*mat
	//glm::mat4 matrix = graphics.camera.getMatrix();
	//axisX = axisX * matrix;
	//axisY = axisY * matrix;

	graphics.camera.rotateAround( (curX-prevX)/4, glm::vec3(0.0f, 0.0f, 0.0f), (glm::vec3)axisY );
//	graphics.camera.rotateAround( (curY-prevY)/4, glm::vec3(0.0f, 0.0f, 0.0f), (glm::vec3)axisX );

	SDL_WarpMouse( gGame.screenWidth/2, gGame.screenHeight/2 );
	
	//if(i)
	//	graphics.camera.rotateAround( 30.0, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

	//	i=0;

	mVManager.update( gGame.dTFloat(), *mSim.mScene );
	mSim.advance( (PxReal)gGame.dTFloat() );
	mSim.mScene->fetchResults(true);
}

void CPracticeState::render() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
/*
	glm::mat4 mat(1.0f);
//move it
	glm::mat4 translate = (glm::mat4)glm::translate( 3.0f, 0.0f, 0.0f);
	mat = mat * translate;
//rotate it
	glm::mat4 rot(1.0f);
	rot = (glm::mat4)glm::rotate( rot, 15.0f, 0.0f, 1.0f, 0.0f );
	mat = rot*mat;

*/
	glEnable(GL_TEXTURE_2D);

	oManager.render( graphics.camera);

	graphics.camera.applyMatrix();

	/*glDisable(GL_TEXTURE_2D);
	glBegin(GL_QUADS);
		glColor3f(1.0f,0.0f,0.0f);
		glNormal3f( 0.0f, 1.0f, 0.0f);
		glVertex3f( 5.0f, 0.0f, -5.0f);
		glVertex3f( -5.0f, 0.0f, -5.0f);
		glVertex3f( -5.0f, 0.0f, 5.0f);
		glVertex3f( 5.0f, 0.0f, 5.0f);
	glEnd();
	*/

	//shape->render( tireTexture, mat );
	/*
glBegin(GL_QUADS);
    // top of cube
    glColor3f(0.0f,0.0f,1.0f);			// Set The Color To Blue
	glNormal3f( 0.0f, 1.0f, 0.0f);
    glVertex3f( 1.0f, 1.0f,-1.0f);		// Top Right Of The Quad (Top)
    glVertex3f(-1.0f, 1.0f,-1.0f);		// Top Left Of The Quad (Top)
    glVertex3f(-1.0f, 1.0f, 1.0f);		// Bottom Left Of The Quad (Top)
    glVertex3f( 1.0f, 1.0f, 1.0f);		// Bottom Right Of The Quad (Top)

	// bottom of cube
   //glColor3f(1.0f,0.5f,0.0f);			// Set The Color To Orange
	glNormal3f( 0.0f, -1.0f, 0.0f);
    glVertex3f( 1.0f,-1.0f, 1.0f);		// Top Right Of The Quad (Bottom)
    glVertex3f(-1.0f,-1.0f, 1.0f);		// Top Left Of The Quad (Bottom)
    glVertex3f(-1.0f,-1.0f,-1.0f);		// Bottom Left Of The Quad (Bottom)
    glVertex3f( 1.0f,-1.0f,-1.0f);		// Bottom Right Of The Quad (Bottom)
    

    // front of cube
	glNormal3f( 0.0f, 0.0f, 1.0f);
    //glColor3f(1.0f,0.0f,0.0f);			// Set The Color To Red
    glVertex3f( 1.0f, 1.0f, 1.0f);		// Top Right Of The Quad (Front)
    glVertex3f(-1.0f, 1.0f, 1.0f);		// Top Left Of The Quad (Front)
    glVertex3f(-1.0f,-1.0f, 1.0f);		// Bottom Left Of The Quad (Front)
    glVertex3f( 1.0f,-1.0f, 1.0f);		// Bottom Right Of The Quad (Front)

    // back of cube.
	glNormal3f( 0.0f, 0.0f, -1.0f);
    //glColor3f(1.0f,1.0f,0.0f);			// Set The Color To Yellow
    glVertex3f( 1.0f,-1.0f,-1.0f);		// Top Right Of The Quad (Back)
    glVertex3f(-1.0f,-1.0f,-1.0f);		// Top Left Of The Quad (Back)
    glVertex3f(-1.0f, 1.0f,-1.0f);		// Bottom Left Of The Quad (Back)
    glVertex3f( 1.0f, 1.0f,-1.0f);		// Bottom Right Of The Quad (Back)

    // left of cube
	glNormal3f( -1.0f, 0.0f, 0.0f);
    //glColor3f(0.0f,0.0f,1.0f);			// Blue
    glVertex3f(-1.0f, 1.0f, 1.0f);		// Top Right Of The Quad (Left)
    glVertex3f(-1.0f, 1.0f,-1.0f);		// Top Left Of The Quad (Left)
    glVertex3f(-1.0f,-1.0f,-1.0f);		// Bottom Left Of The Quad (Left)
    glVertex3f(-1.0f,-1.0f, 1.0f);		// Bottom Right Of The Quad (Left)

    // Right of cube
	glNormal3f( 1.0f, 0.0f, 0.0f);
    //glColor3f(1.0f,0.0f,1.0f);			// Set The Color To Violet
    glVertex3f( 1.0f, 1.0f,-1.0f);	        // Top Right Of The Quad (Right)
    glVertex3f( 1.0f, 1.0f, 1.0f);		// Top Left Of The Quad (Right)
    glVertex3f( 1.0f,-1.0f, 1.0f);		// Bottom Left Of The Quad (Right)
    glVertex3f( 1.0f,-1.0f,-1.0f);		// Bottom Right Of The Quad (Right)
glEnd();
*/

	SDL_GL_SwapBuffers();
}

void CPracticeState::renderLoadingScr( CMyFont& font ) {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glColor3f( 0.15f, 0.0f, 0.0f);

	glBegin(GL_QUADS);
		glVertex3i( 0,0,0);
		glVertex3i( gGame.screenWidth,0,0);
		glVertex3i( gGame.screenWidth,gGame.screenHeight,0);
		glVertex3i( 0,gGame.screenHeight,0);
	glEnd();
	

	glColor3f( 1.0f, 1.0f, 0.0f );
	font.renderTextCentered( gGame.screenWidth/2, gGame.screenHeight/2, "Loading...", 1);

	SDL_GL_SwapBuffers();
}