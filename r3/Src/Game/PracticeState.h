#ifndef _PRACTICESTATE_H_
#define _PRACTICESTATE_H_ 0

#include "State.h"
#include "../Graphics/GUI.h"
#include "ObjectManager.h"
#include "../Physics/Sim.h"
#include "../Physics/VehicleManager.h"

class CPracticeState : public CState {
	public:
	CPracticeState();
	~CPracticeState();

	int loadState();
	void leaveState();

	void handleEvent( SDL_Event* ev );
	void step();
	void render();

	CObjectManager oManager;

	private:
		void renderLoadingScr( CMyFont& font );
		CSim mSim;
		CVehicleManager mVManager;

};

#endif