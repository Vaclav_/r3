#include "State.h"
#include "Game.h"

#ifdef _MSC_VER
	#include <Windows.h>
#endif
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library

extern CGame gGame;

CState::CState(void)
{
}

CState::~CState(void)
{
}


int CState::loadState() { return -1; }

void CState::leaveState() {}

void CState::handleEvent( SDL_Event* ev ) {}

void CState::step() {}

void CState::render() {}

void CState::setNextState( int nState ) {
	nextState = nState;
}

int CState::getNextState() {
	return nextState;
}

//------------------------------------
CMenuState::CMenuState() {}

CMenuState::~CMenuState() {}

int CMenuState::loadState() {

	nextState = MENU;

	graphics.init();

	graphics.load2D();

	font0.load( "fonts/font0.bmp" );

	buttonTexture0 = loadPng( "gfx/button0.png" );
	buttonTexture1 = loadPng( "gfx/button0Pressed.png" );

	int k;
	const char* labels[MAX_NUM_BUTTONS];
	labels[0] = "Start";
	labels[1] = "Settings";
	labels[2] = "Credits";
	labels[3] = "Quit";
	int buttonW = 128;
	int buttonH = 32;
	for( k=0; k<MAX_NUM_BUTTONS; k++) {
		buttons.push_back( CButton() );
		buttons.at(k).setTexture( buttonTexture0, buttonTexture1 );
		buttons.at(k).setFont( &font0 );
		buttons.at(k).setParam( gGame.screenWidth/2-buttonW/2, gGame.screenHeight/2-buttonH/2+buttonH*k-buttonH*MAX_NUM_BUTTONS/2, buttonW, buttonH, labels[k], 1.0f, 1.0f, 1.0f );
	}

	SDL_ShowCursor(SDL_ENABLE);

	//graphics.camera.setPos( glm::vec3( 0, 10, 10) );
	//graphics.camera.setLook( glm::vec3( 0, 0, 0) );

	return 0;
}

void CMenuState::leaveState() {
	font0.freeFont();
	glDeleteTextures(1, &buttonTexture0 );
	glDeleteTextures(1, &buttonTexture1 );
}

void CMenuState::handleEvent( SDL_Event* ev ) {

	unsigned int i;

	switch (ev->type) {
		case SDL_QUIT:
			gGame.setRunning(false);
			break;
		case SDL_KEYDOWN:
			if( ev->key.keysym.sym == SDLK_q )
				buttons.at(BQUIT).pushButton();
			else if( ev->key.keysym.sym == SDLK_4 && BQUIT+1 == 4)
				buttons.at(BQUIT).pushButton();
			else if( ev->key.keysym.sym == SDLK_1 )
				buttons.at(BSTART).pushButton();
			else if( ev->key.keysym.sym == SDLK_2 )
				buttons.at(BSETTINGS).pushButton();
			else if( ev->key.keysym.sym == SDLK_3 )
				buttons.at(BCREDITS).pushButton();
			break;
		case SDL_MOUSEBUTTONDOWN:
			if((ev->button.button) == SDL_BUTTON_LEFT) {
				for( i=0; i<buttons.size(); i++)
					buttons.at(i).buttonDown(ev->button.x,ev->button.y);
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if((ev->button.button) == SDL_BUTTON_LEFT) {
				for( i=0; i<buttons.size(); i++)
					buttons.at(i).buttonUp(ev->button.x,ev->button.y);
			}
			break;
		default:
			break;
	}

}


void CMenuState::step() {
	if( buttons.at(BQUIT).check() )
		gGame.setRunning(false);
	if( buttons.at(BSETTINGS).check() )
		nextState = MENU_SETTINGS;
	if( buttons.at(BCREDITS).check() )
		nextState = CREDITS;
	if( buttons.at(BSTART).check() )
		nextState = S_PRACTICE;
}

void CMenuState::render() {

	unsigned int i;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	for( i=0; i<buttons.size(); i++)
		buttons.at(i).render();

	//glColor3f( 1.0, 0.0, 0.0 );
	//font0.renderText( 400, 400, "Hello World!", 1);

	SDL_GL_SwapBuffers();
}
