#ifndef _GSTATE_H_
#define _GSTATE_H_ 0

#include <SDL/SDL.h>
#include <vector>
#include "../Graphics/Graphics.h"
#include "../Graphics/GUI.h"

enum gStates { MENU, MENU_SETTINGS, S_PRACTICE, CREDITS, MULTIPLAYER };

enum menuButtons { BSTART=0, BSETTINGS, BCREDITS, BQUIT, MAX_NUM_BUTTONS };

class CState {
	public:
	CState();
	~CState();
	virtual int loadState();
	virtual void leaveState();

	virtual void handleEvent( SDL_Event* ev );
	virtual void step();
	virtual void render();

	virtual void setNextState( int nState );
	virtual int getNextState();

	protected:
	int nextState;	//next game state
	CGraphics graphics;

};

class CMenuState : public CState {
	public:
	CMenuState();
	~CMenuState();

	int loadState();
	void leaveState();

	void handleEvent( SDL_Event* ev );
	void step();
	void render();

	private:
	CMyFont font0;
	GLuint buttonTexture0;
	GLuint buttonTexture1;
	std::vector<CButton> buttons;
};

#endif