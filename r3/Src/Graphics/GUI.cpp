#include "GUI.h"
#include "Graphics.h"
#include "../Game/Game.h"

extern CGame gGame;

CMyFont::CMyFont(void) :
	texture(0),
	dispLists(0) {
}


CMyFont::~CMyFont(void) {
}

int CMyFont::load( const char* file ) {

	int Status=FALSE;							   // Status Indicator
	SDL_Surface *TextureImage;			   // Create Storage Space For The Textures
	TextureImage = NULL;

	if (TextureImage=LoadBMP((char*)file)) {
			Status=TRUE;							// Set The Status To TRUE
			glGenTextures(1, &texture);		  // Create Two Texture


			glBindTexture(GL_TEXTURE_2D, texture);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
			glTexImage2D(GL_TEXTURE_2D, 0, 3, TextureImage->w, TextureImage->h, 0, GL_RGB, GL_UNSIGNED_BYTE, TextureImage->pixels);
	}

	if (TextureImage)
		SDL_FreeSurface(TextureImage);		// Free The Image Structure

	float	cx;											// Holds Our X Character Coord
	float	cy;											// Holds Our Y Character Coord
	int loop;

	if(Status) {	//gen lists

		dispLists=glGenLists(256);								// Creating 256 Display Lists
		glBindTexture(GL_TEXTURE_2D, texture);			// Select Our Font Texture
		for (loop=0; loop<256; loop++)						// Loop Through All 256 Lists
		{
			cx=(float)(loop%16)/16.0f;						// X Position Of Current Character
			cy=(float)(loop/16)/16.0f;						// Y Position Of Current Character

			glNewList(dispLists+loop,GL_COMPILE);				// Start Building A List
				glBegin(GL_QUADS);							// Use A Quad For Each Character
					glTexCoord2f(cx,1-cy-0.0625f);			// Texture Coord (Bottom Left)
					glVertex2i(0,16);						// Vertex Coord (Bottom Left)
					glTexCoord2f(cx+0.0625f,1-cy-0.0625f);	// Texture Coord (Bottom Right)
					glVertex2i(16,16);						// Vertex Coord (Bottom Right)
					glTexCoord2f(cx+0.0625f,1-cy);			// Texture Coord (Top Right)
					glVertex2i(16,0);						// Vertex Coord (Top Right)
					glTexCoord2f(cx,1-cy);					// Texture Coord (Top Left)
					glVertex2i(0,0);						// Vertex Coord (Top Left)
				glEnd();									// Done Building Our Quad (Character)
				glTranslated(13,0,0);						// Move To The Right Of The Character
			glEndList();									// Done Building The Display List
		}													// Loop Until All 256 Are Built

	}	//end gen lists

	return !Status;		//return 0 is all went well
}

SDL_Surface* CMyFont:: LoadBMP(char *filename) {
	Uint8 *rowhi, *rowlo;
	Uint8 *tmpbuf, tmpch;
	SDL_Surface *image;
	int i, j;

	image = SDL_LoadBMP(filename);
	if ( image == NULL ) {
		gGame.error("Unable to load file: ");
		gGame.error(filename);
		gGame.error(SDL_GetError());
		return(NULL);
	}

	// GL surfaces are upsidedown and RGB, not BGR
	tmpbuf = (Uint8 *)malloc(image->pitch);
	if ( tmpbuf == NULL ) {
		gGame.error("Out of memory.");
		return(NULL);
	}
	rowhi = (Uint8 *)image->pixels;
	rowlo = rowhi + (image->h * image->pitch) - image->pitch;
	for ( i=0; i<image->h/2; ++i ) {
		for ( j=0; j<image->w; ++j ) {
			tmpch = rowhi[j*3];
			rowhi[j*3] = rowhi[j*3+2];
			rowhi[j*3+2] = tmpch;
			tmpch = rowlo[j*3];
			rowlo[j*3] = rowlo[j*3+2];
			rowlo[j*3+2] = tmpch;
		}
		memcpy(tmpbuf, rowhi, image->pitch);
		memcpy(rowhi, rowlo, image->pitch);
		memcpy(rowlo, tmpbuf, image->pitch);
		rowhi += image->pitch;
		rowlo -= image->pitch;
	}
	free(tmpbuf);

	return(image);
}

void CMyFont::renderText( int x, int y, const char* string, int set) {

	if (set>1)
		set=1;
	else if(set<0)
		set=0;

	//in 2D mode already
	glEnable( GL_TEXTURE_2D );
	glDisable( GL_DEPTH_TEST );
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D, texture);			// Select Our Font Texture
	glLoadIdentity();									// Reset The Projection Matrix
	glTranslated(x,y,0);								// Position The Text (0,0 - Bottom Left)
	glListBase(dispLists-32+(128*set));						// Choose The Font Set (0 or 1)
	glCallLists(strlen(string),GL_BYTE,string);			// Write The Text To The Screen
}

void CMyFont::renderTextCentered( int x, int y, const char* string, int set) {

	if (set>1)
		set=1;
	else if(set<0)
		set=0;

	//in 2D mode already
	glEnable( GL_TEXTURE_2D );
	glDisable( GL_DEPTH_TEST );
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glEnable(GL_BLEND);
	glBindTexture(GL_TEXTURE_2D, texture);			// Select Our Font Texture
	glLoadIdentity();									// Reset The Projection Matrix
	glTranslatef((float)x-(float)strlen(string)/2.0f*13.0f, (float)y-8.0f,0.0f);								// Position The Text (0,0 - Bottom Left)
	glListBase(dispLists-32+(128*set));						// Choose The Font Set (0 or 1)
	glCallLists(strlen(string),GL_BYTE,string);			// Write The Text To The Screen
}

void CMyFont::freeFont(void) {
	glDeleteLists(dispLists,256);
	if( texture != 0 ) {
		glDeleteTextures(1, &texture );
		texture = 0;
	}
}

CButton::CButton() :
	texture(0),
	text(NULL),
	x(0),
	y(0),
	state(BUTTONUP),
	sizeX(0),
	sizeY(0),
	red(1.0f),
	green(0.0f),
	blue(0.0f),
	buttonEvent(false),
	frameCnt(0)
	{
}

CButton::~CButton() {}

//extension must be png
void CButton::setTexture( GLuint texUp, GLuint texDown ) {
	texture = texUp;
	texturePressed = texDown;
}

void CButton::setFont( CMyFont* f ) {
	font = f;
}

void CButton::setParam( int pX, int pY, int sX, int sY, const char* t, float r, float g, float b) {
	x = pX;
	y = pY;
	sizeX = sX;
	sizeY = sY;
	text = t;
	red = r; green = g; blue = b;
}

void CButton::render() {
	
	GLuint renderTexture=0;

	if( state == BUTTONUP )
		renderTexture = texture;
	else
		renderTexture = texturePressed;

	if( renderTexture != 0 ) {
		glDisable( GL_DEPTH_TEST );
		glEnable( GL_TEXTURE_2D );
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
		glColor3f( 1.0f, 1.0f, 1.0f);
		glLoadIdentity();
		glTranslatef( (float)x, (float)y, 0.0f);
		glBindTexture( GL_TEXTURE_2D, renderTexture );
		glBegin(GL_QUADS);
			glTexCoord2i( 0, 0);
			glVertex3i( 0, sizeY, 0);

			glTexCoord2i( 1, 0);
			glVertex3i( sizeX, sizeY, 0);

			glTexCoord2i( 1, 1);
			glVertex3i( sizeX, 0, 0);

			glTexCoord2i( 0, 1);
			glVertex3i( 0, 0, 0);
		glEnd();
	} else gGame.error("Button texture is null!");
	
	if( text != NULL && font != NULL ) {
		glColor3f( red, green, blue);
		font->renderTextCentered( x+sizeX/2, y+sizeY/2, text, 1);
	} else gGame.error("Button font is null!");
}

void CButton::buttonDown( int cX, int cY) {
	if( cX > x && cX < x+sizeX && cY > y && cY < y+sizeY )
		state = BUTTONDOWN;
}

void CButton::buttonUp( int cX, int cY) {
if( cX > x && cX < x+sizeX && cY > y && cY < y+sizeY ) {
		buttonEvent = true;
	}
	state = BUTTONUP;
}

void CButton::pushButton( unsigned int frameTime ) {
	frameCnt = frameTime;
	state = BUTTONDOWN;
}

bool CButton::check() {
	if( frameCnt > 0 ) {
		frameCnt--;
		if ( frameCnt==0 ) {
			state = BUTTONUP;
			buttonEvent = true;
		}
	}
	
	if( buttonEvent ) {
		buttonEvent = false;
		return true;
	} else return false;
}