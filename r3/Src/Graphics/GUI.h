
#ifndef _MY_GUI_H
#define _MY_GUI_H 0

#ifdef _MSC_VER
	#include <Windows.h>
#endif
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library

#include <SDL/SDL.h>

#include "../Game/Game.h"
extern CGame gGame;

enum eButtonState {
	BUTTONUP = 0, BUTTONDOWN
};

class CMyFont
{
public:
	CMyFont(void);
	~CMyFont(void);

	int load( const char* file );
	void renderText( int x, int y, const char* string, int set);
	void renderTextCentered( int x, int y, const char* string, int set);
	void freeFont(void);

private:
	GLuint texture;
	GLuint dispLists;
	SDL_Surface* LoadBMP(char *filename);
};

class CButton {
public:
	CButton();
	~CButton();
	void setTexture( GLuint texUp, GLuint texDown );
	void setFont( CMyFont* f );
	void setParam( int pX, int pY, int sX, int sY, const char* t, float r, float g, float b);
	void buttonDown( int cX, int cY);
	void buttonUp( int cX, int cY);
	void pushButton( unsigned int frameTime = 6*gGame.getFrameRate()/25 );
	
	bool check();
	void render();

private:
	int state;
	bool buttonEvent;
	unsigned int frameCnt;
	GLuint texture, texturePressed;
	CMyFont* font;
	int x, y;
	int sizeX, sizeY;
	const char* text;
	GLfloat red, green, blue;
};

#endif