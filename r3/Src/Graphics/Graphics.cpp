#include "Graphics.h"
#include "../Game/Game.h"
#include "lodepng.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

extern CGame gGame;

CCamera::CCamera(void) {}
CCamera::~CCamera(void) {}

void CCamera::lookAt( glm::vec3 camPos, glm::vec3 lookingAt, glm::vec3 head ) {
		camMatrix = glm::lookAt(
			camPos, // Camera pos
			lookingAt, // Camera looking at
			head  // default: up
		);
}

void CCamera::translate( glm::vec3 vec ) {

	glm::mat4 translate = (glm::mat4)glm::translate( -vec[0], -vec[1], -vec[2]);
	camMatrix = camMatrix * translate;
}

void CCamera::rotate( GLfloat deg, glm::vec3 axis ) {
	glm::mat4 rot(1.0f);
	rot = (glm::mat4)glm::rotate( rot, -deg, axis[0], axis[1], axis[2] );
	camMatrix = rot*camMatrix;
}

void CCamera::rotateAround( GLfloat deg, glm::vec3 lookingAt, glm::vec3 axis) {
	//translate to the origin
	glm::mat4 translate = (glm::mat4)glm::translate( lookingAt[0], lookingAt[1], lookingAt[2]);
	camMatrix = camMatrix*translate;
	//rotate
	glm::mat4 rot(1.0f);
	rot = (glm::mat4)glm::rotate( rot, deg, axis[0], axis[1], axis[2] );
	camMatrix = camMatrix*rot;
	//translate back
	translate = (glm::mat4)glm::translate( -lookingAt[0], -lookingAt[1], -lookingAt[2]);
	camMatrix = camMatrix*translate;
}

void CCamera::applyMatrix() {
	glLoadIdentity();
	glMultMatrixf( &camMatrix[0][0] );
	return;
}

glm::mat4 CCamera::getMatrix() {
	return camMatrix;
}

//--------------------------------

CGraphics::CGraphics(void)
{
}

CGraphics::~CGraphics(void)
{
}

void CGraphics::init() {
	glClearColor( 0, 0, 0, 0 );
}

void CGraphics::load3D() {
	glShadeModel(GL_SMOOTH);
	glViewport( 0, 0, gGame.screenWidth, gGame.screenHeight );

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, gGame.screenWidth / gGame.screenHeight, 0.01, 300.0);
	glMatrixMode(GL_MODELVIEW);

	float light0_pos[] = {5.0, 5.0, 10.0, 0.0};
	glLightfv(GL_LIGHT0, GL_POSITION, light0_pos);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);

	glClearDepth(1.0f);
	glEnable(GL_CULL_FACE);

	glEnable(GL_DEPTH_TEST);

	glDisable(GL_BLEND);
	glDepthFunc(GL_LESS);
}

void CGraphics::load2D() {
	glDisable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glViewport( 0, 0, gGame.screenWidth, gGame.screenHeight );

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, gGame.screenWidth, gGame.screenHeight, 0.0f, -1.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);

	glDisable(GL_LIGHT0);
	glDisable(GL_LIGHTING);
}

GLuint loadPng( const char* filename ) {

	GLuint texture;
	glGenTextures ( 1, &texture );
	glBindTexture ( GL_TEXTURE_2D, texture );

	// Load file and decode image.
	std::vector<unsigned char> image;
	unsigned width, height;
	unsigned error = lodepng::decode(image, width, height, filename);

	// If there's an error, display it.
	if(error != 0)
	{
		gGame.error( lodepng_error_text(error) );
		return 0;
	}

	//image starts from the top left corner, but make image2 start from the bottom left corner for openGL
	std::vector<unsigned char> image2( width * height * 4 );
	size_t max = width*height*4 -1;
	for(size_t y = 0; y < height; y++)
	for(size_t x = 0; x < width; x++)
	for(size_t c = 0; c < 4; c++)
	{
		image2[4 * width * y + 4 * x + c] = image[ max - y*width*4 - (width-x-1)*4 - (3-c) ];
	}

	// Enable the texture for OpenGL.
	glEnable(GL_TEXTURE_2D);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); //GL_NEAREST = no smoothing
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image2[0]);

	return texture;
}
