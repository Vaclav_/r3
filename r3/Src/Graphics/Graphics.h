#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_ 0

#ifdef _MSC_VER
	#include <Windows.h>
#endif
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library

#include <glm/glm.hpp>

class CCamera {
	
public:
	CCamera();
	~CCamera();
	void lookAt( glm::vec3 camPos, glm::vec3 lookingAt, glm::vec3 head = glm::vec3(0,1,0) );
	void translate( glm::vec3 vec );
	void rotate( GLfloat deg, glm::vec3 axis );
	void rotateAround( GLfloat deg, glm::vec3 lookingAt, glm::vec3 axis);

	void applyMatrix();
	glm::mat4 getMatrix();

private:
	glm::mat4 camMatrix;
};

class CGraphics {
public:
	CGraphics(void);
	~CGraphics(void);

	void init();
	void load3D();
	void load2D();

	CCamera camera;

	//renderobject
};

GLuint loadPng( const char* filename );

#endif

