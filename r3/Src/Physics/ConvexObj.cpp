#include "ConvexObj.h"

#include <stdio.h>
#include <stdlib.h>

#include "SampleVehicle_SceneQuery.h"
#include "VehicleUtils.h"

extern void fatalError( const char* error );

CConvexObj::CConvexObj( const char* meshFile, PxPhysics* thePhysics, PxScene* theScene, PxCooking* cooking ) :
	mActor( NULL )
{
	PxVec3 pos( 0.0f, -2.0f, 0.0f);
	mActor = thePhysics->createRigidDynamic(PxTransform(pos));

	/*
	const PxVec3 convexVerts[] =
	{
	PxVec3(20.0, 0.0, 20.0 ),
	PxVec3(20.0, 0.0, -20.0 ),
	PxVec3(-20.0, 0.0, 20.0 ),

	};

	PxConvexMeshDesc convexDesc;
	convexDesc.points.count     = 32;
	convexDesc.points.stride    = sizeof(PxVec3);
	convexDesc.points.data      = convexVerts;
	convexDesc.flags            = PxConvexFlag::eCOMPUTE_CONVEX;
	PxDefaultMemoryOutputStream buf;
	if(!cooking->cookConvexMesh(convexDesc, buf)) {
		fatalError("cookConvexMesh failed!");
		mActor = NULL;
		return;
	}
	PxDefaultMemoryInputData input(buf.getData(), buf.getSize());
	PxConvexMesh* convexMesh = thePhysics->createConvexMesh(input);
	*/
	

	
	PxConvexMesh* convexMesh = loadMesh( meshFile, *thePhysics, *cooking );
	if( convexMesh == NULL )
		return;

	PxMaterial* aMaterial;

	//static friction, dynamic friction, restitution
	aMaterial = thePhysics->createMaterial(0.5f, 0.5f, 0.2f);
	if(!aMaterial) {
		fatalError("createMaterial failed!");
		mActor = NULL;
		return;
	}

	PxShape* aConvexShape = mActor->createShape( PxConvexMeshGeometry(convexMesh), *aMaterial);

	PxFilterData qryFilterData;
	SampleVehicleSetupDrivableShapeQueryFilterData(&qryFilterData); 
	PxFilterData simulationFilterData;
	simulationFilterData.word0=COLLISION_FLAG_GROUND;
	simulationFilterData.word1=COLLISION_FLAG_GROUND_AGAINST;
	aConvexShape->setSimulationFilterData(simulationFilterData);
	aConvexShape->setQueryFilterData( qryFilterData );
	aConvexShape->setFlag(PxShapeFlag::eVISUALIZATION,false);//?
	

	PxReal sphereDensity = 0.5;
	PxRigidBodyExt::updateMassAndInertia(*mActor, sphereDensity);

	PxVec3 vel( 0.0, 0.0, 0.0 );
	mActor->setLinearVelocity( vel );

	mActor->setRigidDynamicFlag(PxRigidDynamicFlag::eKINEMATIC, true);

	theScene->addActor(*mActor);
}


CConvexObj::~CConvexObj(void)
{
}

PxConvexMesh* loadMesh( const char* filename, PxPhysics& thePhysics, PxCooking& cooking ) {
	PxConvexMesh* loadedMesh = NULL;
    FILE* hnd = NULL;
    hnd = fopen( filename, "r+");
    if( hnd == NULL ) {
        fatalError("Can't open model file: ");
		fatalError(filename);
		fatalError("\n");
        return NULL;
    }

    float vert1, vert2, vert3;
	PxVec3 curVertex;
    int i=0;
	char c, c2;
	PxVec3* vertexes = NULL;
	int size=0;

	fscanf( hnd, "%c", &c);
	while( c != 'v' ) {
		while( c!= '\n' ) {	//read the full line
			fscanf( hnd, "%c", &c);
		}
		fscanf( hnd, "%c", &c);	//get the first char of the line
	}
	//first vertex
	fscanf( hnd, "%c", &c2);
	while( c == 'v' && c2 == ' ' ) {
		i++;
		vertexes = (PxVec3*) realloc( vertexes, i*sizeof(PxVec3) );
		fscanf( hnd, "%f", &vert1);
		fscanf( hnd, "%f", &vert2);
		fscanf( hnd, "%f", &vert3);
		curVertex[0] = vert1;
		curVertex[1] = vert2;
		curVertex[2] = vert3;
		/*
		printf("%f ", vert1);
		printf("%f ", vert2);
		printf("%f\n", vert3); */
		*(vertexes+i-1) = curVertex;
		fscanf( hnd, "%c", &c);	//for '\n'
		fscanf( hnd, "%c", &c);
		fscanf( hnd, "%c", &c2);
	}

	fclose(hnd);

	//printf("i: %i\n" , i);

	PxConvexMeshDesc convexDesc;
	convexDesc.points.count     = i;
	convexDesc.points.stride    = sizeof(PxVec3);
	convexDesc.points.data      = vertexes;
	convexDesc.flags            = PxConvexFlag::eCOMPUTE_CONVEX;
	PxDefaultMemoryOutputStream buf;
	if(!cooking.cookConvexMesh(convexDesc, buf)) {
		fatalError("cookConvexMesh failed!");
		return NULL;
	}
	PxDefaultMemoryInputData input(buf.getData(), buf.getSize());
	loadedMesh = thePhysics.createConvexMesh(input);
	if( loadedMesh == NULL )
		fatalError("loadedMesh is NULL");

	return loadedMesh;
}