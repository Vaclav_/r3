#ifndef CONV_OBJ_H
#define CONV_OBJ_H

#include <PxPhysicsAPI.h>
using namespace physx;

class CConvexObj
{
public:
	PxRigidDynamic* mActor;

public:
	CConvexObj( const char* meshFile, PxPhysics* thePhysics, PxScene* theScene, PxCooking* cooking );
	~CConvexObj(void);
};

PxConvexMesh* loadMesh( const char* filename, PxPhysics& thePhysics, PxCooking& cooking );


#endif