#include "Sim.h"
#include "SampleVehicle_SceneQuery.h"
#include "VehicleUtils.h"

static PxDefaultErrorCallback gDefaultErrorCallback;
static PxDefaultAllocator gDefaultAllocatorCallback;

extern void fatalError( const char* error );
extern void debug( const char* msg );

PxPhysics* CSim::mPhysics = NULL;

CSim::CSim(void) : 
	mFoundation( NULL ),
	mProfileZoneManager( NULL ),
	mScene( NULL ),
	mCpuDispatcher ( NULL ),
	mNbThreads( 1 ),
	mCooking( NULL ),
	mPVDConnection( NULL ),
#ifdef PX_WINDOWS
	mCudaContextManager( NULL ),
#endif
	mAccumulator( 0.0f )
	//,mStepSize( 1.0f/100.0f )
{

	mFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback);
	if(!mFoundation) {
		fatalError("PxCreateFoundation failed!");
		return;
	}

	bool recordMemoryAllocations = true;
	mProfileZoneManager = &PxProfileZoneManager::createProfileZoneManager(mFoundation);
	if(!mProfileZoneManager) {
		fatalError("PxProfileZoneManager::createProfileZoneManager failed!");
		return;
	}

	mPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *mFoundation,
				PxTolerancesScale(), recordMemoryAllocations, mProfileZoneManager );
	if(!mPhysics) {
		fatalError("PxCreatePhysics failed!");
		return;
	}

	
	mCooking = PxCreateCooking(PX_PHYSICS_VERSION, *mFoundation, PxCookingParams());
	if (!mCooking) {
		fatalError("PxCreateCooking failed!");
		return;
	}
	

	if (!PxInitExtensions(*mPhysics)) {
		fatalError("PxInitExtensions failed!");
		return;
	}

    //PxRegisterArticulations(*physics);
    //PxRegisterHeightFields(*physics);

	PxSceneDesc sceneDesc(mPhysics->getTolerancesScale());
	sceneDesc.gravity = PxVec3(0.0f, -9.81f, 0.0f);
	//customizeSceneDesc(sceneDesc);

	if(!sceneDesc.cpuDispatcher)
	{
		mCpuDispatcher = PxDefaultCpuDispatcherCreate(mNbThreads);
		if(!mCpuDispatcher) {
			fatalError("PxDefaultCpuDispatcherCreate failed!");
			return;
		}
		sceneDesc.cpuDispatcher    = mCpuDispatcher;
	}
	if(!sceneDesc.filterShader)
		sceneDesc.filterShader    = SampleVehicleFilterShader;//&PxDefaultSimulationFilterShader;


#ifdef PX_WINDOWS
	if(!sceneDesc.gpuDispatcher && mCudaContextManager)
	{
		sceneDesc.gpuDispatcher = mCudaContextManager->getGpuDispatcher();
		debug("Loaded GPU Dispatcher.");
	} else {
		debug("No GPU Dispatcher.");
	}
#endif
	
	mScene = mPhysics->createScene(sceneDesc);
	if (!mScene) {
		fatalError("createScene failed!");
		return;
	}

	PxInitVehicleSDK( *mPhysics );

}

CSim::~CSim(void)
{
}


bool CSim::advance(PxReal dt) {
    /*mAccumulator  += dt;
    if(mAccumulator < mStepSize)
        return false;

    mAccumulator -= mStepSize;*/

    mScene->simulate( dt );
    return true;
}

int CSim::onInit() {

	return 0;
}

void CSim::createActors(void) {
	PxMaterial* aMaterial = mPhysics->createMaterial(0.5f, 0.5f, 0.5f);    //static friction, dynamic friction, restitution
	if(!aMaterial)
		fatalError("createMaterial failed!");

	PxReal radius = 0.5;
	PxVec3 pos(-5.0, 5.0, 0.0);
	PxReal density = 1.0;
	//PxSphereGeometry(radius),
	PxRigidDynamic* aSphereActor =  PxCreateDynamic(*mPhysics, PxTransform(pos), PxBoxGeometry(0.5f,0.5f,0.5f),
            *aMaterial, density);
	PxVec3 vel(2.0, 0.0, 4.0);
	aSphereActor->setLinearVelocity(vel);

	PxShape* shapes[1];

	aSphereActor->getShapes(shapes, 1, 0);

	PxFilterData qryFilterData;
	SampleVehicleSetupNonDrivableShapeQueryFilterData(&qryFilterData); 
	PxFilterData simulationFilterData;
	simulationFilterData.word0=COLLISION_FLAG_OBSTACLE;
	simulationFilterData.word1=COLLISION_FLAG_OBSTACLE_AGAINST;
	shapes[0]->setSimulationFilterData(simulationFilterData);
	shapes[0]->setQueryFilterData( qryFilterData );

	mScene->addActor(*aSphereActor);

	//create a plane
	/*
	PxRigidStatic* plane = PxCreatePlane(*mPhysics, PxPlane(PxVec3(0,1,0), 0), *aMaterial);
	if (!plane)
		fatalError("Couldn't create plane!");
	mScene->addActor(*plane);
	*/
}

int CSim::PVDInit(void) {

	// check if PvdConnection manager is available on this platform
	if(mPhysics->getPvdConnectionManager() == NULL) {
		fatalError("PvdConnection manager is not available.");
		return (-1);
	}

	// setup connection parameters
	const char*     pvd_host_ip = "127.0.0.1";  // IP of the PC which is running PVD
	int             port        = 5425;         // TCP port to connect to, where PVD is listening
	unsigned int    timeout     = 100;          // timeout in milliseconds to wait for PVD to respond,
												// consoles and remote PCs need a higher timeout.
	PxVisualDebuggerConnectionFlags connectionFlags = PxVisualDebuggerExt::getAllConnectionFlags();

	// and now try to connect
	mPVDConnection = PxVisualDebuggerExt::createConnection(mPhysics->getPvdConnectionManager(),
		pvd_host_ip, port, timeout, connectionFlags);
	
	return 0;
}

void CSim::release(void) {
	// remember to release the connection by manual in the end
	if (mPVDConnection)
		mPVDConnection->release();

	PxCloseVehicleSDK();
	
	mPhysics->release();
	mFoundation->release();
}


PxFilterFlags SampleVehicleFilterShader(	
	PxFilterObjectAttributes attributes0, PxFilterData filterData0, 
	PxFilterObjectAttributes attributes1, PxFilterData filterData1,
	PxPairFlags& pairFlags, const void* constantBlock, PxU32 constantBlockSize)
{
	PX_FORCE_PARAMETER_REFERENCE(constantBlock);
	PX_FORCE_PARAMETER_REFERENCE(constantBlockSize);

	// let triggers through
	if(PxFilterObjectIsTrigger(attributes0) || PxFilterObjectIsTrigger(attributes1))
	{
		pairFlags = PxPairFlag::eTRIGGER_DEFAULT;
		return PxFilterFlags();
	}



	// use a group-based mechanism for all other pairs:
	// - Objects within the default group (mask 0) always collide
	// - By default, objects of the default group do not collide
	//   with any other group. If they should collide with another
	//   group then this can only be specified through the filter
	//   data of the default group objects (objects of a different
	//   group can not choose to do so)
	// - For objects that are not in the default group, a bitmask
	//   is used to define the groups they should collide with
	if ((filterData0.word0 != 0 || filterData1.word0 != 0) &&
		!(filterData0.word0&filterData1.word1 || filterData1.word0&filterData0.word1))
		return PxFilterFlag::eSUPPRESS;

	pairFlags = PxPairFlag::eCONTACT_DEFAULT;

	//enable CCD stuff -- for now just for everything or nothing.
	if((filterData0.word3|filterData1.word3) & SWEPT_INTEGRATION_LINEAR)
		pairFlags |= PxPairFlag::eSWEPT_INTEGRATION_LINEAR;

	// The pairFlags for each object are stored in word2 of the filter data. Combine them.
	pairFlags |= PxPairFlags(PxU16(filterData0.word2 | filterData1.word2));
	return PxFilterFlags();
}