#ifndef SIM_H
#define SIM_H

#include <PxPhysicsAPI.h>

using namespace physx;

enum Word3
{
	SWEPT_INTEGRATION_LINEAR = 1,
};

PxFilterFlags SampleVehicleFilterShader(	
	PxFilterObjectAttributes attributes0, PxFilterData filterData0, 
	PxFilterObjectAttributes attributes1, PxFilterData filterData1,
	PxPairFlags& pairFlags, const void* constantBlock, PxU32 constantBlockSize);

class CSim {
public:
	PxFoundation* mFoundation;
	PxProfileZoneManager* mProfileZoneManager;
	static PxPhysics* mPhysics;
	PxScene* mScene;
	PxDefaultCpuDispatcher* mCpuDispatcher;
	PxU32 mNbThreads;
	PxReal mAccumulator;
	//PxReal mStepSize;
	PxCooking* mCooking;
	PVD::PvdConnection* mPVDConnection;
#ifdef PX_WINDOWS
	pxtask::CudaContextManager*	mCudaContextManager;
#endif

public:
	CSim(void);
	~CSim(void);
	int onInit(void);
	bool advance(PxReal dt);
	void createActors(void);
	int PVDInit(void);
	void release(void);
};


#endif

