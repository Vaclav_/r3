#include "Vehicle.h"
#include "ConvexObj.h"

void fatalError( const char* error );

CVehicle::CVehicle( void ) : 
	mCar( NULL ),
	mSurfaceTirePairs( NULL )
{
}

CVehicle::~CVehicle(void)
{
}

int CVehicle::create(PxScene& theScene, PxPhysics& thePhysics, PxCooking& cooking, const PxMaterial& material,
		 const PxF32 chassisMass, PxF32 wheelMass, const PxVec3* wheelCentreOffsets4, PxConvexMesh** chassisConvexMeshes, const PxU32 numChassisMeshes,
		 const PxTransform* chassisLocalPoses, PxConvexMesh** wheelConvexMeshes4, const PxTransform& startTransform, const bool useAutoGearFlag) {

	PxVehicleWheelsSimData* wheelsSimData=PxVehicleWheelsSimData::allocate(4);
	PxVehicleDriveSimData4W driveSimData;

	PxVehicleChassisData chassisData;

	createVehicle4WSimulationData( chassisMass, chassisConvexMeshes[0], wheelMass,
		wheelConvexMeshes4, wheelCentreOffsets4, *wheelsSimData, driveSimData, chassisData);

	PxRigidDynamic* vehActor = createVehicleActor4W( chassisData, wheelConvexMeshes4,
		chassisConvexMeshes, numChassisMeshes, chassisLocalPoses,theScene, thePhysics, material);

	//Create a car.
	mCar = PxVehicleDrive4W::allocate(4);
	mCar->setup( &thePhysics, vehActor, *wheelsSimData, driveSimData, 0);

	//Free the sim data because we don't need that any more.
	wheelsSimData->free();

	//Don't forget to add the actor to the scene.
	theScene.addActor(*vehActor);

	resetNWCar(startTransform, mCar);

	//Set the autogear mode of the instantiate car.
	mCar->mDriveDynData.setUseAutoGears(useAutoGearFlag);
	
	return 0;
}




