#ifndef MVEHICLE_H
#define MVEHICLE_H

#include <vector>
#include "VehicleUtils.h"
#include <PxPhysicsAPI.h>

using namespace physx;

class CVehicle
{
public:
	PxVehicleDrive4W* mCar;
	PxVehicleDrivableSurfaceToTireFrictionPairs* mSurfaceTirePairs;

public:
	CVehicle(void);
	~CVehicle(void);
	int create(PxScene& theScene, PxPhysics& thePhysics, PxCooking& cooking, const PxMaterial& material,
		 const PxF32 chassisMass, PxF32 wheelMass, const PxVec3* wheelCentreOffsets4, PxConvexMesh** chassisConvexMeshes, const PxU32 numChassisMeshes,
		 const PxTransform* chassisLocalPoses, PxConvexMesh** wheelConvexMeshes4, const PxTransform& startTransform, const bool useAutoGearFlag);

};


#endif