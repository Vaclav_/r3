#ifndef VEHICLE_CONTROLLER_H
#define VEHICLE_CONTROLLER_H

#include "common/PxPhysXCommon.h"
#include "foundation/PxVec3.h"
#include "vehicle/PxVehicleSDK.h"
#include "vehicle/PxVehicleUtilControl.h"


using namespace physx;

class CVehicleController
{
public:



	CVehicleController();
	~CVehicleController();

	void setCarKeyboardInputs
		(const bool accel, const bool brake, const bool handbrake, 
		 const bool steerleft, const bool steerright, 
		 const bool gearup, const bool geardown)
	{
		mKeyPressedAccel=accel;
		mKeyPressedBrake=brake;
		mKeyPressedHandbrake=handbrake;
		mKeyPressedSteerLeft=steerleft;
		mKeyPressedSteerRight=steerright;
		mKeyPressedGearUp=gearup;
		mKeyPressedGearDown=geardown;
	}

	void setCarGamepadInputs
		(const PxF32 accel, const PxF32 brake, 
		 const PxF32 steer, 
		 const bool gearup, const bool geardown, 
		 const bool handbrake)
	{
		mGamepadAccel=accel;
		mGamepadCarBrake=brake;
		mGamepadCarSteer=steer;
		mGamepadGearup=gearup;
		mGamepadGeardown=geardown;
		mGamepadCarHandbrake=handbrake;
	}

	void toggleAutoGearFlag() 
	{
		mToggleAutoGears = true;
	}

	void update(const PxF32 dtime, PxVehicleWheels& focusVehicle);

	void clear();

private:

	//Raw driving inputs - keys
	bool			mKeyPressedAccel;
	bool			mKeyPressedBrake;
	bool			mKeyPressedHandbrake;
	bool			mKeyPressedSteerLeft;
	bool			mKeyPressedSteerRight;
	bool			mKeyPressedGearUp;
	bool			mKeyPressedGearDown;

	//Raw driving inputs - gamepad 
	PxF32			mGamepadAccel;
	PxF32			mGamepadCarBrake;
	bool			mGamepadCarHandbrake;
	bool			mGamepadGearup;
	bool			mGamepadGeardown;
	PxF32			mGamepadCarSteer;

	//Record and replay using raw driving inputs.
	bool			mRecord;
	bool			mReplay;
	enum
	{
		MAX_NUM_RECORD_REPLAY_SAMPLES=8192
	};
	// Keyboard
	bool			mKeyboardAccelValues[MAX_NUM_RECORD_REPLAY_SAMPLES];
	bool			mKeyboardBrakeValues[MAX_NUM_RECORD_REPLAY_SAMPLES];
	bool			mKeyboardHandbrakeValues[MAX_NUM_RECORD_REPLAY_SAMPLES];
	bool			mKeyboardSteerLeftValues[MAX_NUM_RECORD_REPLAY_SAMPLES];
	bool			mKeyboardSteerRightValues[MAX_NUM_RECORD_REPLAY_SAMPLES];
	bool			mKeyboardGearupValues[MAX_NUM_RECORD_REPLAY_SAMPLES];
	bool			mKeyboardGeardownValues[MAX_NUM_RECORD_REPLAY_SAMPLES];
	// Gamepad 
	PxF32			mGamepadAccelValues[MAX_NUM_RECORD_REPLAY_SAMPLES];
	bool			mGamepadGearupValues[MAX_NUM_RECORD_REPLAY_SAMPLES];
	bool			mGamepadGeardownValues[MAX_NUM_RECORD_REPLAY_SAMPLES];
	PxF32			mGamepadCarBrakeValues[MAX_NUM_RECORD_REPLAY_SAMPLES];
	PxF32			mGamepadCarSteerValues[MAX_NUM_RECORD_REPLAY_SAMPLES];
	bool			mGamepadCarHandbrakeValues[MAX_NUM_RECORD_REPLAY_SAMPLES];

	PxU32			mNumSamples;
	PxU32			mNumRecordedSamples;

	// Raw data taken from the correct stream (live input stream or replay stream)
	bool			mUseKeyInputs;

	// Toggle autogears flag on focus vehicle
	bool			mToggleAutoGears;

	//Auto-reverse mode.
	bool			mIsMovingForwardSlowly;
	bool			mInReverseMode;

	//Update 
	void processRawInputs(const PxF32 timestep, const bool useAutoGears, PxVehicleDrive4WRawInputData& rawInputData);
	void processAutoReverse(
		const PxVehicleWheels& focusVehicle, const PxVehicleDriveDynData& driveDynData, const PxVehicleDrive4WRawInputData& rawInputData, 
		bool& toggleAutoReverse, bool& newIsMovingForwardSlowly) const;

	////////////////////////////////
	//Record and replay deprecated at the moment.
	//Setting functions as private to avoid them being used.
	///////////////////////////////
	bool getIsInRecordReplayMode() const {return (mRecord || mReplay);}
	bool getIsRecording() const {return mRecord;}
	bool getIsReplaying() const {return mReplay;}

	void enableRecordReplayMode()
	{
		PX_ASSERT(!getIsInRecordReplayMode());
		mRecord=true;
		mReplay=false;
		mNumRecordedSamples=0;
	}

	void disableRecordReplayMode()
	{
		PX_ASSERT(getIsInRecordReplayMode());
		mRecord=false;
		mReplay=false;
		mNumRecordedSamples=0;
	}

	void restart();
	////////////////////////////

};


#endif