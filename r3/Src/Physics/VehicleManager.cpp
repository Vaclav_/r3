#include "VehicleManager.h"
#include "VehicleUtils.h"
#include <iostream>
#include "ConvexObj.h"
#include "SampleVehicle_SceneQuery.h"

extern void fatalError( const char* error );

CVehicleManager::CVehicleManager( PxPhysics& thePhysics ) :
	mNumVehicles( 1 ),
	mSqWheelRaycastBatchQuery( NULL ),
	mSurfaceTirePairs( NULL )
	{

	mSqData = SampleVehicleSceneQueryData::allocate(MAX_NUM_4W_VEHICLES*4);

	createStandardMaterials( thePhysics );

	mSurfaceTirePairs = PxVehicleDrivableSurfaceToTireFrictionPairs::create(MAX_NUM_TIRE_TYPES,MAX_NUM_SURFACE_TYPES, (const PxMaterial**)mStandardMaterials, mVehicleDrivableSurfaceTypes);
	for(PxU32 i=0;i<MAX_NUM_SURFACE_TYPES;i++)
	{
		for(PxU32 j=0;j<MAX_NUM_TIRE_TYPES;j++)
		{
			mSurfaceTirePairs->setTypePairFriction(i,j,gTireFrictionMultipliers[i][j]);
		}
	}

}


CVehicleManager::~CVehicleManager(void)
{
}

void CVehicleManager::createStandardMaterials( PxPhysics& thePhysics )
{
	PxF32 restitutions[MAX_NUM_SURFACE_TYPES] = {0.2f, 0.2f, 0.2f, 0.2f};
	PxF32 staticFrictions[MAX_NUM_SURFACE_TYPES] = {0.5f, 0.5f, 0.5f, 0.5f};
	PxF32 dynamicFrictions[MAX_NUM_SURFACE_TYPES] = {0.5f, 0.5f, 0.5f, 0.5f};

	for(PxU32 i=0;i<MAX_NUM_SURFACE_TYPES;i++) 
	{
		//Create a new material.
		mStandardMaterials[i] = thePhysics.createMaterial(staticFrictions[i], dynamicFrictions[i], restitutions[i]);
		//if(!mStandardMaterials[i])
		//{
		//	getSampleErrorCallback().reportError(PxErrorCode::eINTERNAL_ERROR, "createMaterial failed", __FILE__, __LINE__);
		//}

		//Set up the drivable surface type that will be used for the new material.
		mVehicleDrivableSurfaceTypes[i].mType = i;
	}
}

int j=0;
//PxQuat rot[11];

void CVehicleManager::update( PxF32 timeStep, PxScene& theScene ) {

	PxShape* carShapes[11]; //!!! make this one to be dynamically allocated, carShapes[numChassisMeshes+4]
	const PxVehicleWheels& vehicle = *myVehicle.mCar;
	const PxU32 numShapes=vehicle.getRigidDynamicActor()->getNbShapes();
	vehicle.getRigidDynamicActor()->getShapes( carShapes, numShapes);
	
	/*
	if( !(j%25) ) {
		float a = myVehicle.mCar->mDriveDynData.getEngineRotationSpeed();
		printf("E speed: %f\n", a);
	}
	*/


	controller.update( timeStep, *vehicles[0]);

        //Create a scene query if we haven't already done so.
        if(NULL==mSqWheelRaycastBatchQuery)
        {
                mSqWheelRaycastBatchQuery=mSqData->setUpBatchedSceneQuery( &theScene );
        }
        //Raycasts.
        PxVehicleSuspensionRaycasts(mSqWheelRaycastBatchQuery, mNumVehicles, vehicles, mSqData->getRaycastQueryResultBufferSize(), mSqData->getRaycastQueryResultBuffer());
	
	PxVehicleUpdates( timeStep, PxVec3( 0.0f, -9.81f, 0.0f), *mSurfaceTirePairs, mNumVehicles, vehicles);
}

void CVehicleManager::create( PxPhysics& thePhysics, PxScene& theScene, PxCooking& cooking) {

	PxMaterial* aMaterial;
	//static friction, dynamic friction, restitution
	aMaterial = thePhysics.createMaterial(0.5f, 0.5f, 0.1f);
	if(!aMaterial) {
		fatalError("CreateMaterial failed!");
		return;
	}

	PxF32 wheelMass = 20.0f;
	PxF32 chassisMass = 1500.0f;

	PxVec3 wheelCentreOffsets4[4] = {	//FL, FR, RL, RR
		PxVec3(-0.822839f, -0.429795f, 1.407236f),
		PxVec3(0.822839f, -0.429795f, 1.407236f),
		PxVec3(-0.822839f, -0.429795f, -1.343002f),
		PxVec3(0.822839f, -0.429795f, -1.343002f) };

	PxConvexMesh* chassisConvexMeshes[7];
	chassisConvexMeshes[0] = loadMesh( "gfx/chassis_middle.obj", thePhysics, cooking );
	chassisConvexMeshes[1] = loadMesh( "gfx/chassis_top.obj", thePhysics, cooking );
	chassisConvexMeshes[2] = loadMesh( "gfx/chassis_bottom1.obj", thePhysics, cooking );
	chassisConvexMeshes[3] = loadMesh( "gfx/chassis_bottom2.obj", thePhysics, cooking );
	chassisConvexMeshes[4] = loadMesh( "gfx/chassis_bottom3.obj", thePhysics, cooking );
	chassisConvexMeshes[5] = loadMesh( "gfx/chassis_bottom4.obj", thePhysics, cooking );
	chassisConvexMeshes[6] = loadMesh( "gfx/chassis_bottom5.obj", thePhysics, cooking );
	if( chassisConvexMeshes[0] == NULL || chassisConvexMeshes[1] == NULL || chassisConvexMeshes[2] == NULL ||
		chassisConvexMeshes[3] == NULL || chassisConvexMeshes[4] == NULL || chassisConvexMeshes[5] == NULL ||
		chassisConvexMeshes[6] == NULL ) {
		fatalError("chassis mesh is null");
		return;
	}

	PxU32 numChassisMeshes = 7;

	PxTransform chassisLocalPoses[7];
	chassisLocalPoses[0] = PxTransform( PxVec3(0.0f, 0.153937f, 0.068693f) );
	chassisLocalPoses[1] = PxTransform( PxVec3(0.0f, 0.556081f, -0.276419f) );
	chassisLocalPoses[2] = PxTransform( PxVec3(0.0f, -0.130277f, 2.092338f) );
	chassisLocalPoses[3] = PxTransform( PxVec3(0.0f, -0.145744f, 1.396082f) );
	chassisLocalPoses[4] = PxTransform( PxVec3(0.0f, -0.144163f, 0.02696f) );
	chassisLocalPoses[5] = PxTransform( PxVec3(0.0f, -0.145744f, -1.338403f) );
	chassisLocalPoses[6] = PxTransform( PxVec3(0.0f, -0.144089f, -1.96925f) );

	PxConvexMesh* wheelConvexMeshes4[4];
	wheelConvexMeshes4[0] = loadMesh( "gfx/tire.obj", thePhysics, cooking );
	wheelConvexMeshes4[1] = wheelConvexMeshes4[0];
	wheelConvexMeshes4[2] = wheelConvexMeshes4[0];
	wheelConvexMeshes4[3] = wheelConvexMeshes4[0];
	if( wheelConvexMeshes4[0] == NULL ) {
		fatalError("wheel mesh is null");
		return;
	}

	bool useAutoGearFlag = true;

	myVehicle.create( theScene, thePhysics, cooking, *aMaterial, chassisMass, wheelMass,
		wheelCentreOffsets4, chassisConvexMeshes, numChassisMeshes, chassisLocalPoses, wheelConvexMeshes4,
		 PxTransform( PxVec3(0.0f, 4.0f, 0.0f)), useAutoGearFlag);

	vehicles[0] = myVehicle.mCar;
}

