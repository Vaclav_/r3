#ifndef VEHICLE_MANAGER_H
#define VEHICLE_MANAGER_H

#include "Vehicle.h"
#include <PxPhysicsAPI.h>
#include "VehicleController.h"
using namespace physx;

enum
{
	MAX_NUM_INDEX_BUFFERS = 16
};

enum
{
	MAX_NUM_4W_VEHICLES = 1
};



class SampleVehicleSceneQueryData;

class CVehicleManager

{
public:
	CVehicle myVehicle;
	PxVehicleWheels* vehicles[1];
	PxU32 mNumVehicles;

	CVehicleController controller;

	PxVehicleDrivableSurfaceToTireFrictionPairs* mSurfaceTirePairs;
	PxVehicleDrivableSurfaceType	mVehicleDrivableSurfaceTypes[MAX_NUM_INDEX_BUFFERS];
	PxMaterial*						mStandardMaterials[MAX_NUM_INDEX_BUFFERS];

	PxBatchQuery* mSqWheelRaycastBatchQuery;
	SampleVehicleSceneQueryData* mSqData;



public:
	CVehicleManager( PxPhysics& thePhysics );
	~CVehicleManager(void);
	void create( PxPhysics& thePhysics, PxScene& theScene, PxCooking& cooking);

	void createStandardMaterials( PxPhysics& thePhysics );
	void update( PxF32 timeStep, PxScene& theScene );
};

#endif

