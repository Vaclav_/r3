#ifndef VEHICLE_UTILS_H
#define VEHICLE_UTILS_H

#include <vector>
#include <PxPhysicsAPI.h>
using namespace physx;
#include "SampleVehicle_SceneQuery.h"

enum tire_types
{
	TIRE_TYPE_WETS=0,
	TIRE_TYPE_SLICKS,
	TIRE_TYPE_ICE,
	TIRE_TYPE_MUD,
	MAX_NUM_TIRE_TYPES
};

//Drivable surface types.
enum
{
	SURFACE_TYPE_MUD=0,
	SURFACE_TYPE_TARMAC,
	SURFACE_TYPE_SNOW,
	SURFACE_TYPE_GRASS,
	MAX_NUM_SURFACE_TYPES
};

enum collision_flags
{
	COLLISION_FLAG_GROUND			=	1 << 0,
	COLLISION_FLAG_WHEEL			=	1 << 1,
	COLLISION_FLAG_CHASSIS			=	1 << 2,
	COLLISION_FLAG_OBSTACLE			=	1 << 3,
	COLLISION_FLAG_DRIVABLE_OBSTACLE=	1 << 4,

	COLLISION_FLAG_GROUND_AGAINST	=															COLLISION_FLAG_CHASSIS | COLLISION_FLAG_OBSTACLE | COLLISION_FLAG_DRIVABLE_OBSTACLE,
	COLLISION_FLAG_WHEEL_AGAINST	=									COLLISION_FLAG_WHEEL |	COLLISION_FLAG_CHASSIS | COLLISION_FLAG_OBSTACLE,
	COLLISION_FLAG_CHASSIS_AGAINST	=			COLLISION_FLAG_GROUND | COLLISION_FLAG_WHEEL |	COLLISION_FLAG_CHASSIS | COLLISION_FLAG_OBSTACLE | COLLISION_FLAG_DRIVABLE_OBSTACLE,
	COLLISION_FLAG_OBSTACLE_AGAINST	=			COLLISION_FLAG_GROUND | COLLISION_FLAG_WHEEL |	COLLISION_FLAG_CHASSIS | COLLISION_FLAG_OBSTACLE | COLLISION_FLAG_DRIVABLE_OBSTACLE,
	COLLISION_FLAG_DRIVABLE_OBSTACLE_AGAINST=	COLLISION_FLAG_GROUND 						 |	COLLISION_FLAG_CHASSIS | COLLISION_FLAG_OBSTACLE | COLLISION_FLAG_DRIVABLE_OBSTACLE,
};

//Tire model friction for each combination of drivable surface type and tire type.
static PxF32 gTireFrictionMultipliers[MAX_NUM_SURFACE_TYPES][MAX_NUM_TIRE_TYPES]=
{
	//WETS	SLICKS	ICE		MUD
	{0.95f,	0.95f,	0.95f,	0.95f},		//MUD
	{1.10f,	1.15f,	1.10f,	1.10f},		//TARMAC
	{0.70f,	0.70f,	0.70f,	0.70f},		//ICE
	{0.80f,	0.80f,	0.80f,	0.80f}		//GRASS
};

/*
//Make sure that suspension raycasts only consider shapes flagged as drivable that don't belong to the owner vehicle.
enum driveable
{
	SAMPLEVEHICLE_DRIVABLE_SURFACE = 0xffff0000,
	SAMPLEVEHICLE_UNDRIVABLE_SURFACE = 0x0000ffff
};*///moved to scenequery.h

PxVec3 computeChassisAABBDimensions( PxConvexMesh* chassisConvexMesh );

void computeWheelWidthsAndRadii( PxConvexMesh** wheelConvexMeshes, PxF32* wheelWidths, PxF32* wheelRadii);

void createVehicle4WSimulationData
(const PxF32 chassisMass, PxConvexMesh* chassisConvexMesh,
 const PxF32 wheelMass, PxConvexMesh** wheelConvexMeshes, const PxVec3* wheelCentreOffsets,
 PxVehicleWheelsSimData& wheelsData, PxVehicleDriveSimData4W& driveData, PxVehicleChassisData& chassisData);

PxRigidDynamic* createVehicleActor4W
(const PxVehicleChassisData& chassisData,
 PxConvexMesh** wheelConvexMeshes, PxConvexMesh** chassisConvexMeshes, const PxU32 numChassisMeshes, 
 const PxTransform* chassisLocalPoses, PxScene& scene, PxPhysics& physics, const PxMaterial& material);

void setupActor
(PxRigidDynamic* vehActor,
 const PxFilterData& vehQryFilterData,
 const PxConvexMeshGeometry** wheelGeometries, const PxTransform* wheelLocalPoses, const PxU32 numWheelGeometries, const PxMaterial* wheelMaterial, const PxFilterData& wheelCollFilterData,
 const PxGeometry** chassisGeometries, const PxTransform* chassisLocalPoses, const PxU32 numChassisGeometries, const PxMaterial* chassisMaterial, const PxFilterData& chassisCollFilterData,
 const PxVehicleChassisData& chassisData,
 PxPhysics* physics
 );

//void SampleVehicleSetupVehicleShapeQueryFilterData(PxFilterData* qryFilterData);

void resetNWCar(const PxTransform& startTransform, PxVehicleWheels* vehWheels);

#endif
