// r3.cpp : Defines the entry point for the console application.

#ifdef _MSC_VER
	// Including SDKDDKVer.h defines the highest available Windows platform.
	#include <SDKDDKVer.h>
#endif

#include <iostream>

#include "Game/State.h"
#include "Game/Game.h"
#include "Game/PracticeState.h"

CGame gGame;

int main(int argc, char* argv[])
{

	CState* curState = NULL;
	SDL_Event SDLEvent;
	CTimer debugTimer;
	unsigned int nextFrameTime=0;
	unsigned int debugFrameNo=0;
	unsigned int debugSecNo=0;
	const unsigned int FPS_DEBUG_TIME = 1000;

	gGame.init();

	while( gGame.isRunning() ) {

		//create new gamestate
		switch( gGame.activeState ) {
			case MENU:
				gGame.debug("New state: MENU");
				curState = new CMenuState;
				curState->loadState();
				break;
			case MENU_SETTINGS:
				gGame.error("SETTINGS is not yet implemented.");
				gGame.activeState = MENU;
				curState = new CMenuState;
				curState->loadState();
				break;
			case S_PRACTICE:
				curState = new CPracticeState;
				curState->loadState();
				break;
			case CREDITS:
				gGame.error("CREDITS is not yet implemented.");
				gGame.activeState = MENU;
				curState = new CMenuState;
				curState->loadState();
				break;
			default:
				gGame.error("Unknown gamestate, can't be loaded.", -1);
				break;
		}

		debugTimer.start();
		//init value
		nextFrameTime = SDL_GetTicks();


		//while we stay in the same state, and the game is running
		while( gGame.isRunning() && gGame.activeState == curState->getNextState() ) {

			//the time in SDL_Ticks when the next frame comes, until that we have time for this frame
			nextFrameTime += gGame.dTInt();

			while( SDL_PollEvent( &SDLEvent ) ) {
				curState->handleEvent( &SDLEvent );
			}

			curState->step();

			//Draw if we have enough time for it
			if( SDL_GetTicks() < nextFrameTime ) {
				curState->render();
			} else gGame.debug("Skipping frame...");

			//regulating FPS
			if( SDL_GetTicks() < nextFrameTime )
				debugTimer.delay( nextFrameTime - SDL_GetTicks() );

			
			//measuring and debugging FPS
			debugFrameNo++;
			if( debugTimer.getTime() >= 1000 ) {
				debugSecNo++;
				if( debugSecNo >= FPS_DEBUG_TIME/1000) {
					gGame.debug( "FPS: ", debugFrameNo);
					debugSecNo=0;
				}
				debugFrameNo=0;
				debugTimer.start();
			}
			////FPS debugging over

		}	//end while in current state

		if( curState != NULL ) {
			//set the name of the next state
			gGame.activeState = curState->getNextState();
			//delete current gamestate, to create the next in the next loop
			curState->leaveState();
			gGame.debug("Deleting last gamestate...");
			delete curState;
			curState = NULL;
		}

	}	//end while( gGame.isRunning() )

	gGame.end();

	//char c[2];
	//std::cin >> c;

	return 0;
}


